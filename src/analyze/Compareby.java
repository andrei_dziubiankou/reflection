package analyze;

/**
 * @author Andrei Aziubiankou
 *  enum for attribute compareby in @Equal
 */
public enum Compareby {
    /**
     * to determine objects equality by reference
     */
    REFERENCE,
    /**
     * to determine objects equality by value
     */
    VALUE
}
