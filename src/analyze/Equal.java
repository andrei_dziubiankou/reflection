package analyze;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Andrei Aziubiankou
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Equal {
    /**
     * @return by value or reference
     */
    Compareby compareby();
}
