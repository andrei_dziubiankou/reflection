package analyze;

/**
 * @author Andrei Aziubiankou
 *
 */
public class TestClass {
    @Equal(compareby = Compareby.VALUE)
    private int a;
    private int b;
    @Equal(compareby = Compareby.VALUE)
    private String c;
    @Equal (compareby = Compareby.REFERENCE)
    private String d;

    /**
     * @param a the a
     * @param b the b
     * @param c the c
     * @param d the d
     */
    public TestClass(final int a, final int b, final String c, final String d) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    /**
     * @return the a
     */
    public int getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(final int a) {
        this.a = a;
    }

    /**
     * @return the b
     */
    public int getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(final int b) {
        this.b = b;
    }

    /**
     * @return the c
     */
    public String getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(final String c) {
        this.c = c;
    }

    /**
     * @return the d
     */
    public String getD() {
        return d;
    }

    /**
     * @param d the d to set
     */
    public void setD(final String d) {
        this.d = d;
    }

}
