package analyze;

import java.lang.reflect.Field;

/**
 * @author Andrei Aziubiankou
 *
 */
public final class Analyzer {
    private Analyzer() {
        //don't to use
    }
    /**
     * @param a first object
     * @param b second objecdt
     * @return true if objects of same class have equal values or references in @Equal-fields
     */
    public static boolean equalObjects(final Object a, final Object b) {
        Class<?> classA = a.getClass();
        Class<?> classB = b.getClass();
        if (classA == classB) {
            Field[] fields = classA.getDeclaredFields();
            for (Field field: fields) {
                if (field.isAnnotationPresent(Equal.class)) {
                    Equal anno = field.getAnnotation(Equal.class);
                    field.setAccessible(true);
                    Object fieldValueA = null;
                    Object fieldValueB = null;
                    try {
                        fieldValueA = field.get(a);
                        fieldValueB = field.get(b);
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    Compareby compareBy = anno.compareby();
                    switch (compareBy) {
                    case VALUE:
                        if (!fieldValueA.equals(fieldValueB)) {
                            return false;
                        }
                        break;
                    case REFERENCE:
                        if (!(fieldValueA == fieldValueB)) {
                            return false;
                        }
                        break;
                    default:
                        break;
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
