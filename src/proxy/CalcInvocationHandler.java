package proxy;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import calculators.Calculator;

/**
 * @author Andrei Dziubiankou
 *
 */
public class CalcInvocationHandler implements InvocationHandler {
    private Calculator calc;

    /**
     * default constructor for calculate-handler
     */
    public CalcInvocationHandler() {
        super();
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        // TODO Auto-generated method stub
        System.out.println("proxy invoked");
        Field fil = calc.getClass().getDeclaredField("a");
        fil.setAccessible(true);
        int change = 15;
        System.out.println("a = " + fil.getInt(calc) + ", set a = " + change);
        fil.set(calc, change);
        return method.invoke(calc, args);
    }

}
