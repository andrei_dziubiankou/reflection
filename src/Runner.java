import analyze.Analyzer;
import analyze.TestClass;
import calculators.Calculator;
import calculators.CalculatorFactory;

/**
 * @author Andrei Aziubiankou
 *
 */
public final class Runner {
    private Runner() {
        //don't to be used
    }

    /**
     * @param args for console
     */
    public static void main(final String[] args) {
        //demonstration of task part A
        try {
            System.out.println("Demonstaration of part A (a = 6; b = 3):");
            Calculator calc = CalculatorFactory.getInstanceOf(Class.forName("calculators.PlusCalculator"));
            calc.calculate();
            calc = CalculatorFactory.getInstanceOf(Class.forName("calculators.MinusCalculator"));
            calc.calculate();
            calc = CalculatorFactory.getInstanceOf(Class.forName("calculators.MultiplyCalculator"));
            calc.calculate();
            calc = CalculatorFactory.getInstanceOf(Class.forName("calculators.DivideCalculator"));
            calc.calculate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("\n Demonstaration of part B");
        String world = "Wolrd"; //string for test comparebly by reference
        TestClass firstObj = new TestClass(5, 15, "Hello", world);
        TestClass secondObj = new TestClass(5, 10, "Hello", world);
        System.out.println("Objects are equal: ");
        System.out.println(Analyzer.equalObjects(firstObj, secondObj));
    }

}
