package calculators;

import proxy.Proxy;

/**
 * @author Andrei Dziubiankou
 *
 */
@Proxy (invocationHandler = "proxy.CalcInvocationHandler")
public class DivideCalculator implements Calculator {
    private int a = 6;
    private int b = 3;
    @Override
    public void calculate() {
        System.out.println("a / b = " + (a / b));
    }

}
