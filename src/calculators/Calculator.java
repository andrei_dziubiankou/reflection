package calculators;

/**
 * @author Andrei Dziubiankou
 *
 */
public interface Calculator {
    /**
     * do calculating
     */
    void calculate();
}
