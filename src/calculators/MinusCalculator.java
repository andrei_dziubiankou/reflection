package calculators;

import proxy.Proxy;

/**
 * @author Andrei Dziubiankou
 *
 */
@Proxy (invocationHandler = "proxy.CalcInvocationHandler")
public class MinusCalculator implements Calculator {
    private int a = 6;
    private int b = 3;

    @Override
    public void calculate() {
        // TODO Auto-generated method stub
        System.out.println("a - b = " + (a - b));
    }

}
