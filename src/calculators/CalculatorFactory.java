package calculators;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author Andrei Dziubiankou
 *
 */
public final class CalculatorFactory {
    /**
     * @param calcClass className of calculator
     * @return instance of calculator
     */
    private CalculatorFactory() {
        //don't to be used
    }

    /**
     * @param calcClass class for object Calc
     * @return Calc object
     */
    public static Calculator getInstanceOf(final Class<?> calcClass) {
        Calculator calc = null;
        try {
            System.out.println(calcClass.getSimpleName());
            calc = (Calculator) calcClass.newInstance();
             if (calcClass.isAnnotationPresent(proxy.Proxy.class)) {
                 System.out.println("proxy is present");
                 proxy.Proxy an = calcClass.getAnnotation(proxy.Proxy.class);
                 String handlerClassName = an.invocationHandler();
                 InvocationHandler handler = (InvocationHandler) Class.forName(handlerClassName).newInstance();
                 Field realCalc = handler.getClass().getDeclaredField("calc");
                 realCalc.setAccessible(true);
                 realCalc.set(handler, calc);
                 Object proxy = Proxy.newProxyInstance(calc.getClass().getClassLoader(), new Class[] {Calculator.class},
                         handler);
                 return (Calculator) proxy;
             }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException
                | ClassNotFoundException | NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return calc;
    }
}
